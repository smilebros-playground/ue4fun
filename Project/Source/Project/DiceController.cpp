// Fill out your copyright notice in the Description page of Project Settings.

#include "DiceController.h"
#include "Dice.h"
#include <Engine/World.h>

DEFINE_LOG_CATEGORY_STATIC(Controller, Log, All);

ADiceController::ADiceController()
{
}

void ADiceController::BeginPlay()
{
    Super::BeginPlay();
    UE_LOG(Controller, Log, TEXT("Controller::BeginPlay()"));

    auto world = GetWorld();
    world->SpawnActor<ADice>(FVector(200, 0, 200), FRotator(0));
}
