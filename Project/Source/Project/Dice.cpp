// Fill out your copyright notice in the Description page of Project Settings.

#include "Dice.h"
#include <Components/TextRenderComponent.h>
#include <Engine/Font.h>
#include <UObject/ConstructorHelpers.h>
#include <Components/StaticMeshComponent.h>

DEFINE_LOG_CATEGORY_STATIC(Dice, Log, All);

// Sets default values
ADice::ADice()
{
    // Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;
    // auto mesh = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("StaticMesh'/Game/Cube.Cube'"));
    // auto cube = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMashComponent0"));

    // cube->SetStaticMesh(mesh.Object);
    // RootComponent = cube;

    auto textr = CreateDefaultSubobject<UTextRenderComponent>(TEXT("TextRenderComponent0"));
    UObject *obj_ptr = StaticLoadObject(UFont::StaticClass(), NULL, TEXT("Font'/Game/NewFont.NewFont'"));
    textr->SetFont(Cast<UFont>(obj_ptr));
    textr->SetText(FText::FromString(TEXT(" \uf788")));
    textr->SetMaterial(0, ConstructorHelpers::FObjectFinder<UMaterialInterface>(TEXT("Material'/Game/MyTextMaterialOpaque.MyTextMaterialOpaque'")).Object);
    RootComponent = textr;
    // textr->SetupAttachment(cube);
}

// Called when the game starts or when spawned
void ADice::BeginPlay()
{
    UE_LOG(Dice, Log, TEXT("Dice::BeginPlay()"));
    Super::BeginPlay();
}

// Called every frame
void ADice::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void ADice::SetupPlayerInputComponent(UInputComponent *PlayerInputComponent)
{
    Super::SetupPlayerInputComponent(PlayerInputComponent);
}
