// Fill out your copyright notice in the Description page of Project Settings.

#include "ProjectGameModeBase.h"
#include "DiceController.h"
#include <Engine/World.h>

DEFINE_LOG_CATEGORY_STATIC(GameModeBase, Log, All);

AProjectGameModeBase::AProjectGameModeBase()
{
    PlayerControllerClass = ADiceController::StaticClass();
}
